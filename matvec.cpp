#include <iostream>
#include <cmath>
#include <unistd.h>
#include "RaplMeter.h"
#include <vector>
#include <cstdlib>
#include <climits>
#include <chrono>
#include <algorithm>
#include <omp.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

using std::vector;

class Matrix {
	int n;
	int m;
	vector<double> values;
	vector<int> cols;
	vector<int> row_start;
	vector<int> row_end;
public:
	Matrix(int n, int m);
	void randomize(int non_zero_rows, double non_zero_prob);
	void mul_vec(vector<double> &res, vector<double> x);
	void print();
};

	Matrix::Matrix(int n, int m) {
		this->n = n;
		this->m = m;
		this->row_start.resize(n,0);
		this->row_end.resize(n,0);
	}

void Matrix::randomize(int non_zero_rows, double non_zero_prob) {
	int n_values = (int)std::round(non_zero_rows * non_zero_prob * this->m);
	this->values.reserve(n_values);
	this->cols.reserve(n_values);
	for(int i = 0; i < n_values; i++) {
		this->values.push_back(((double)rand())/RAND_MAX);
	}
	int last_end = 0;
	for(int i = 0; i < this->n; i++) {
		if(i < non_zero_rows) {
			int remaining_vals = n_values - last_end;
			int remaining_rows = non_zero_rows - i;
			this->row_start[i] = last_end;
			this->row_end[i] = last_end + remaining_vals / remaining_rows;
			last_end = this->row_end[i];
		} else {
			this->row_start[i] = n_values;
			this->row_end[i] = n_values;
		}
	}
	vector<int> perm;
	perm.resize(this->m, 0);
	std::generate(perm.begin(), perm.end(), [n = 0] () mutable { return n++; });
	for(int i = 0; i < non_zero_rows; i++) {
		std::random_shuffle(perm.begin(), perm.end());
		std::sort(perm.begin(), perm.begin() + this->row_end[i] - this->row_start[i]);
		for(int j = 0; j < this->row_end[i]-this->row_start[i]; j++) {
			this->cols.push_back(perm[j]);
		}
	}
}

void Matrix::mul_vec(vector<double> &res, vector<double> x) {
	res.resize(this->n, 0.0);
	int numthreads = omp_get_max_threads();
#pragma omp parallel for schedule(static, 1)
	for(int i = 0; i < this->n; i++) {
		for(int j = this->row_start[i]; j < this->row_end[i]; j++) {
			res[i] += this->values[j] * x[this->cols[j]];
		}
	}
}

void Matrix::print() {
	int idx = 0;
	for(int i = 0; i < this->n; i++) {
		for(int j = 0; j < this->m; j++) {
			if (this->row_start[i] <= idx and idx < this->row_end[i] and this->cols[idx] == j) {
				std::cout << this->values[idx++] << "\t";
			} else {
				std::cout << 0 << "\t";
			}
		}
		std::cout << std::endl;
	}

}

int main(int argc, char **argv) {

	int rseed;
	int n, m;
	int non_zero_rows;
	double non_zero_prob;

	n = atoi(argv[1]);
	m = atoi(argv[2]);
	non_zero_rows = atoi(argv[3]);
	non_zero_prob = atof(argv[4]);
	rseed = atoi(argv[5]);
	
	srand(rseed);
	Matrix A(n,m);
	vector<double> x;
	vector<double> res;
	for(int i = 0; i < m; i++) {
		x.push_back(((double)rand())/INT_MAX);
	}
	A.randomize(non_zero_rows, non_zero_prob);
	// A.print();
  RaplMeter rp;

  rp.init();
  rp.reset();
	auto start = std::chrono::high_resolution_clock::now();
	A.mul_vec(res,x);
  rp.sample();
	auto end = std::chrono::high_resolution_clock::now();
	long time = (std::chrono::duration_cast<std::chrono::nanoseconds>(end-start)).count();
	std::cerr << "res: ";
	for(int i = 0; i < n; i++) {
		std::cerr << res[i] << " ";
	}
	std::cerr  << std::endl;
	std::cout << omp_get_max_threads() << ", " << n << ", " << m << ", " << non_zero_rows << ", " << non_zero_prob << ", " << rp.get_psys_energy() << ", " << rp.get_pkg_energy() << ", " << rp.get_cores_energy() << ", " << rp.get_ram_energy() << ", " << time << std::endl;

}

