#include "mpi.h"
#include <iostream>
#include <cmath>
#include <unistd.h>
#include "RaplMeter.h"

int main()
{
  int rank;
  int size;


  MPI_Status status;
  MPI_Comm comm;
  comm = MPI_COMM_WORLD;

  MPI_Init(NULL, NULL);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  RaplMeter rp;

  rp.init();
  rp.reset();

  sleep(1);

  rp.sample();

  std::cout << "rank " << rank << " pkg: " << rp.get_pkg_energy() << " ram: " << rp.get_ram_energy() << std::endl;

  MPI_Finalize();
}
